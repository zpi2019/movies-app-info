# movies-app-info

## Web Application with movies recommendations

### Project Assumption:
- Progressive Web App with Node.js backend and GraphQL
- Connected to postgreSQL database
- With recommendation service written in Python

### TODO:
- Finish backend
- App interfaces in PWA
- Logging by Facebook/Gmail
- App deployment on heroku
- Data sources for recomendations research


### Current progress:
07.03.2019:
- GraphQL research
- Backend setup with GraphQL in Node.js
- Database server setup on Azure
- PostgreSQL setup on server
- App deployment research(decided to use Heroku)
- Blockchain usage research
- Pre-work PWA research